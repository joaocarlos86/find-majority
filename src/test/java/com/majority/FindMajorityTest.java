package com.majority;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FindMajorityTest {

    private FindMajority findMajority;

    @BeforeEach
    public void setup() {
        findMajority = new FindMajority();
    }

    @Test
    void findNajority_givenArray_shouldReturnMajority(){
        Integer[] data = {1, 1, 1, 1, 2, 2, 2, 1};
        Integer majority = findMajority.findMajority(data);

        assertEquals(1, majority);

        Integer[] data2 = {1, 1, 1, 1, 2, 2, 2, 2,2,2,2,2, 1};
        majority = findMajority.findMajority(data2);

        assertEquals(2, majority);
    }

    @Test
    void findNajorityOptimized_givenArray_shouldReturnMajority(){
        Integer[] data = {1, 1, 2, 2, 2, 1, 1};
        Integer majority = findMajority.findMajorityOptimized(data);

        assertEquals(1, majority);

        Integer[] data2 = {1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1};
        majority = findMajority.findMajorityOptimized(data2);

        assertEquals(2, majority);
    }


    @Test
    void findNajorityOptimized_givenArrayBigEnoughArrays_shoulBeFasterThenNonOptimizedVersion(){
        Integer[] data = new Integer[100000000];

        IntStream.range(0, data.length).forEach(i -> data[i] = (int)(Math.random() * 100));

        Date now = new Date();
        final Integer majorityCalculatedByNonOptimizedAlgorithm = findMajority.findMajorityOptimized(data);
        Date after = new Date();
        final Long runtimeOfNonOptimizedAlgorithm = after.getTime() - now.getTime();
        assertEquals(majorityCalculatedByNonOptimizedAlgorithm.intValue(), majorityCalculatedByNonOptimizedAlgorithm);

        now = new Date();
        final Integer majorityCalculatedByOptimizedAlgorithm = findMajority.findMajorityOptimized(data);
        after = new Date();

        final Long runtimeOfOptimizedAlgorithm = after.getTime() - now.getTime();
        assertEquals(majorityCalculatedByOptimizedAlgorithm, majorityCalculatedByNonOptimizedAlgorithm);
        assertTrue(runtimeOfOptimizedAlgorithm < runtimeOfNonOptimizedAlgorithm);
    }
}