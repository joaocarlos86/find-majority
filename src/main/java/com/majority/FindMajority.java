package com.majority;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class FindMajority {

    /**
     * Finds the the element that happens most times in a given array.
     *
     * Time complexity: O(n(log n)) because of the sort operation in the map.
     *
     * @param data
     * @return
     */
    public Integer findMajority(Integer[] data) {

        final Map<Integer, Integer> occurrences = new HashMap<>();

        for(Integer element : data) {
            Integer elementOccurrences = occurrences.get(element);
            if(elementOccurrences != null) {
                elementOccurrences++;
                occurrences.put(element, elementOccurrences);
            } else {
                occurrences.put(element, 1);
            }
        }

        return occurrences
            .entrySet()
            .stream()
                .sorted(Map.Entry.comparingByValue(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer integer, Integer t1) {
                        return t1.compareTo(integer);
                    }
                }))
                .findFirst()
                .get()
                .getKey();

    }

    /**
     * Finds the the element that happens most times in a given array.
     *
     * Time Complexity: O(n)
     *
     * @param data
     * @return
     */
    public Integer findMajorityOptimized(Integer[] data) {

        Integer[] occurrences = new Integer[data.length];
        Integer majority = null;
        for(Integer element : data) {
            Integer elementOccurrences = occurrences[element];
            if(elementOccurrences != null) {
                elementOccurrences++;
                occurrences[element] = elementOccurrences;
            } else {
                occurrences[element] = 1;
            }

            if(majority == null) {
                majority = element;
            } else {
                if(occurrences[element] > occurrences[majority]) {
                    majority = element;
                }
            }
        }

        return majority;

    }

}
